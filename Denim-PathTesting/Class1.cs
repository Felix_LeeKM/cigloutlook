﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Net;
using System.Text.RegularExpressions;

namespace Denim_PathTesting
{
    public class pathTest : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> TargetFile { get; set; }
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> TargetText { get; set; }
        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> PathDir { get; set; }
        [Category("Output")]
        [RequiredArgument]
        public OutArgument<bool> FileFound { get; set; }
        [Category("Output")]
        [RequiredArgument]
        public OutArgument<bool> Result { get; set; }
        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> ErrMsg { get; set; }

        private bool isTested = false;
        private bool isTestPassed = false; 

        private bool IsCheck(CodeActivityContext context)
        {
            if (!isTested)
            {
                try
                {
                    String mainFilename = Directory.GetCurrentDirectory().ToString() + @"\" + TargetFile.Get(context);
                    String mainContent = File.ReadAllText(mainFilename);
                    isTestPassed = mainContent.Contains(TargetText.Get(context));
                    isTested = true;
                }
                catch (Exception e)
                {
                    isTestPassed = false;
                    isTested = true;
                    ErrMsg.Set(context, "IsCheck error:" + e.Message);
                }

            }
            return isTestPassed;
        }

        protected override void Execute(CodeActivityContext context)
        {
            ErrMsg.Set(context, "No Error");

            PathDir.Set(context, Directory.GetCurrentDirectory().ToString());
            FileFound.Set(context, File.Exists(Directory.GetCurrentDirectory().ToString() + @"\" + TargetFile.Get(context))); //Main.xaml
            Result.Set(context, IsCheck(context));
        }
    }

}
