﻿using Denim_CustActivity;
using System;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace ConsoleApplication1
{
    class Program
    {

        

        public static void Main(string[] args)
        {

            //var msgPath = MsgPath.Get(context);
            //var savePath = SavePath.Get(context);
            var msgPath = @"D:\HKWuMHA\Desktop\mail\";
            var savePath = @"D:\HKWuMHA\Desktop\mail\";
            List<string> attachList = new List<string>();
            List<string> relateList = new List<string>();
            List<string[]> relationList = new List<string[]>();
            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.MailItem mail;
            string PR_MAIL_HEADER_TAG = "http://schemas.microsoft.com/mapi/proptag/0x1035001F";
            //string PR_MAIL_HEADER_TAG = "http://schemas.microsoft.com/mapi/proptag/0x007D001E";
            Regex regex = new Regex(@"image(\d{3})(?:\.jpg|\.png)");

            bool result = true;
            List<string> lt_mail = new List<string>();

            Console.WriteLine("1st");
            int marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (!relationList.Where(r => r[0] == file.Replace(msgPath, "")).Any())
                {
                    if (file.ToLower().EndsWith(".msg"))
                    {
                        Console.WriteLine("Processed file: " + file);
                        relationList.Add(new string[] { file.Replace(msgPath, ""), marker.ToString() });
                        mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;

                        Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                        string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                        Console.WriteLine("strHeader: " + strHeader);
                        lt_mail.Add(file + strHeader);
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                Console.WriteLine("Save: " + attch.FileName);
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, marker + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("file find: " + file.Replace(msgPath, ""));
                        relationList.Add(new string[] { file.Replace(msgPath, ""), marker.ToString() });
                    }
                    marker++;
                }
            }

            Console.WriteLine("2nd");
            marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (file.ToLower().EndsWith(".msg"))
                {
                    Console.WriteLine("Processed file: " + file);
                    mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;

                    Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                    string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                    if (lt_mail.IndexOf(file + strHeader) == -1)
                    {
                        Console.WriteLine("strHeader: " + strHeader);
                        lt_mail.Add(file + strHeader);
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                Console.WriteLine("Save: " + attch.FileName);
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, relationList.Where(r => r[0] == file.Replace(msgPath, "")).Select(r => r[1]).First() + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                }
                marker++;
            }

            Console.WriteLine("3rd");
            marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (file.ToLower().EndsWith(".msg"))
                {
                    Console.WriteLine("Processed file: " + file);
                    mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;
                    Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                    string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                    if (lt_mail.IndexOf(file + strHeader) == -1)
                    {
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                //System.Diagnostics.Debug.WriteLine("Save: " + attch.FileName);
                                Console.WriteLine("Save: " + attch.FileName);
                                if (attch.FileName.ToLower().EndsWith(".msg"))
                                {
                                    result = false;
                                }
                                if (!result)
                                {
                                    break;
                                }
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, relationList.Where(r => r[0] == file.Replace(msgPath, "")).Select(r => r[1]).First() + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                }
                marker++;
            }

            //Valid.Set(context, result);
            Console.WriteLine(result);
            foreach (var arr in relationList)
            {
                attachList.Add(arr[0]);
                relateList.Add(arr[1]);
                Console.WriteLine(string.Join(",", arr.ToArray()));
            }

            //AttachList.Set(context, attachList);
            Console.ReadLine();
        }        
    }
}
