﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTimeConvert dc = new DateTimeConvert();

            string str = "2020-01-31 13:00";

            string[] arr = {
                "jeudi 30 août 2018 11:49"
                //,"2018-12-02 19:20:58"
                //,"2017年12月24日 16:46:46"
                //,"02 January 2019 14:42"
                //,"16 January 2019 17:55:35 (UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi",
                //"星期三, 1月 2, 2019 12:17 下午"
                //,"9 March 2018 at 5:01:53 PM HKT" //"dd MMMM yyyy 'at' h:mm:ss tt 'HKT'"
                //,"Tuesday, 13 March 2018 at 6:55 PM",
                //"2018年5月30日(星期三) 下午2:15"

            };

            foreach (var df in arr)
            {
                Console.WriteLine("{0}<=>{1}", df, dc.Execute(df));
                //var dt = DateTime.Parse(str);
                //System.Diagnostics.Debug.WriteLine(dt.ToString(df));
                //Console.WriteLine(dt.ToString(df));
            }

            //Console.WriteLine(DateTime.Now.ToString("dddd, dd MMMM yyyy 'at' H:mm tt"));

            DateTime dateTime = DateTime.MaxValue ;

            str = "jeudi 30 août 2018 11:49";

            CultureInfo culture = new CultureInfo("fr-CA");

            var bl = DateTime.TryParseExact(str, "'jeudi' d m yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateTime);



            Console.WriteLine(bl);

            //Console.WriteLine(dc.Execute(str));

            Console.ReadLine();
        }
    }
}
