﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Denim_CustActivity
{
    class PDFLib
    {
        private readonly string sourcePDFpath;
        private readonly string outputPDFpath;

        public PDFLib(string inputFilename, string outputFilename, string sourceDir, string aimDir)
        {
            sourcePDFpath = Path.Combine(sourceDir, inputFilename);
            outputPDFpath = Path.Combine(aimDir, outputFilename);
        }
        
        public void DecryptPdfWithPage(string password, int startpage, int endpage)
        {
            PdfReader reader;
            Document sourceDocument;
            PdfCopy pdfCopyProvider;
            PdfImportedPage importedPage;

            if (string.IsNullOrEmpty(password))
            {
                reader = new PdfReader(sourcePDFpath);
            }
            else
            {
                reader = new PdfReader(sourcePDFpath, new System.Text.ASCIIEncoding().GetBytes(password));
            }

            sourceDocument = new Document(reader.GetPageSizeWithRotation(startpage));
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPDFpath, System.IO.FileMode.Create));

            sourceDocument.Open();

            //Console.WriteLine(reader.NumberOfPages);

            if (reader.NumberOfPages < endpage)
            {
                endpage = reader.NumberOfPages;
            }

            for (int i = startpage; i <= endpage; i++)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, i);
                pdfCopyProvider.AddPage(importedPage);
            }

            sourceDocument.Close();
            pdfCopyProvider.Close();
            reader.Close();
        }
    }
}
