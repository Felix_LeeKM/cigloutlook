﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Net;
using System.Text.RegularExpressions;

namespace Denim_CustActivity
{
    public class readMSG : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> MsgPath { get; set; }//MSG full Path

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> SavePath { get; set; }//Attachment save path

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<bool> Valid { get; set; }//MSG attachment check result

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<List<string>> AttachList { get; set; }//MSG attachment check result

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<List<string>> RelateList { get; set; }//MSG attachment relationship

        protected override void Execute(CodeActivityContext context)
        {
            var msgPath = MsgPath.Get(context);
            var savePath = SavePath.Get(context);
            List<string> attachList = new List<string>();
            List<string> relateList = new List<string>();
            List<string[]> relationList = new List<string[]>();
            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.MailItem mail;
            string PR_MAIL_HEADER_TAG = "http://schemas.microsoft.com/mapi/proptag/0x1035001F";
            //string PR_MAIL_HEADER_TAG = "http://schemas.microsoft.com/mapi/proptag/0x007D001E";

            Regex regex = new Regex(@"image(\d{3})(?:\.jpg|\.png)");

            bool result = true;
            List<string> lt_mail = new List<string>();

            Console.WriteLine("1st");
            int marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (!relationList.Where(r => r[0] == file.Replace(msgPath, "")).Any())
                {
                    if (file.ToLower().EndsWith(".msg"))
                    {
                        Console.WriteLine("Processed file: " + file);
                        relationList.Add(new string[] { file.Replace(msgPath, ""), marker.ToString() });
                        mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;

                        Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                        string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                        Console.WriteLine("strHeader: " + strHeader);
                        lt_mail.Add(file + strHeader);
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                Console.WriteLine("Save: " + attch.FileName);
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, marker + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("file find: " + file.Replace(msgPath, ""));
                        relationList.Add(new string[] { file.Replace(msgPath, ""), marker.ToString() });
                    }
                    marker++;
                }
            }

            Console.WriteLine("2nd");
            marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (file.ToLower().EndsWith(".msg"))
                {
                    Console.WriteLine("Processed file: " + file);
                    mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;

                    Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                    string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                    if (lt_mail.IndexOf(file + strHeader) == -1)
                    {
                        Console.WriteLine("strHeader: " + strHeader);
                        lt_mail.Add(file + strHeader);
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                Console.WriteLine("Save: " + attch.FileName);
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, relationList.Where(r => r[0] == file.Replace(msgPath, "")).Select(r => r[1]).First() + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                }
                marker++;
            }

            Console.WriteLine("3rd");
            marker = 0;
            foreach (string file in Directory.GetFiles(msgPath))
            {
                if (file.ToLower().EndsWith(".msg"))
                {
                    Console.WriteLine("Processed file: " + file);
                    mail = outlookApp.CreateItemFromTemplate(file) as Microsoft.Office.Interop.Outlook.MailItem;
                    Outlook.PropertyAccessor oPropAccessor = mail.PropertyAccessor;
                    string strHeader = (string)oPropAccessor.GetProperty(PR_MAIL_HEADER_TAG);
                    if (lt_mail.IndexOf(file + strHeader) == -1)
                    {
                        int inmarker = 0;
                        foreach (Outlook.Attachment attch in mail.Attachments)
                        {
                            if (!relationList.Where(r => r[0] == attch.FileName).Any() && !regex.IsMatch(attch.FileName))
                            {
                                //System.Diagnostics.Debug.WriteLine("Save: " + attch.FileName);
                                Console.WriteLine("Save: " + attch.FileName);
                                if (attch.FileName.ToLower().EndsWith(".msg"))
                                {
                                    result = false;
                                }
                                if (!result)
                                {
                                    break;
                                }
                                attch.SaveAsFile(savePath + attch.FileName); //Save attachment
                                relationList.Add(new string[] { attch.FileName, relationList.Where(r => r[0] == file.Replace(msgPath, "")).Select(r => r[1]).First() + "-" + inmarker });
                                inmarker++;
                            }
                        }
                    }
                }
                marker++;
            }

            Valid.Set(context, result);
            Console.WriteLine("result: " + result);
            foreach (var arr in relationList)
            {
                attachList.Add(arr[0]);
                relateList.Add(arr[1]);
                Console.WriteLine(string.Join(",", arr.ToArray()));
            }
            RelateList.Set(context, relateList);
            AttachList.Set(context, attachList);
        }
    }

    public class ocrSoap : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Url { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Task { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string[]> Content { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> Result { get; set; }

        [Category("Output")]
        public OutArgument<string> Description { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                //URL Need to be config by the  Environment
                String[] strArr = Content.Get(context);
                OCRTaskService taskService = new OCRTaskService(Url.Get(context));
                PustTaskResult result;
                //if (Task.Get(context).Equals("PushFieldsCaptureTask"))
                if (Task.Get(context).Contains("PushFieldsCaptureTask"))
                {
                    CaptureType cp;
                    if (strArr[1].ToLower().Contains("fc"))
                    {
                        cp = CaptureType.FC;
                    }
                    else
                    {
                        cp = CaptureType.Excel;
                    }
                    //result = taskService.PushFieldsCaptureTask(strArr[0], cp, strArr[2], strArr[3]);
                    result = taskService.PushFieldsCaptureTaskV2(strArr[0], cp, strArr[2], strArr[3],strArr[4]);
                }
                else
                {
                    OCRType ot;
                    bool bo;
                    if (strArr[1].ToLower().Contains("none"))
                    {
                        ot = OCRType.None;
                    }
                    else if (strArr[1].ToLower().Contains("fulltext"))
                    {
                        ot = OCRType.FullText;
                    }
                    else
                    {
                        ot = OCRType.OCRPageRange;
                    }

                    if (strArr[4].ToLower().Equals("true"))
                    {
                        bo = true;
                    }
                    else
                    {
                        bo = false;
                    }
                    //result = taskService.PushOCRTask(strArr[0], ot, Int32.Parse(strArr[2]), Int32.Parse(strArr[3]), bo, strArr[5], strArr[6]);
                    result = taskService.PushOCRTaskV2(strArr[0], ot, Int32.Parse(strArr[2]), Int32.Parse(strArr[3]), bo, strArr[5], strArr[6],strArr[7]);
                }
                Result.Set(context, result.Code.ToString());
                Description.Set(context, result.Description.ToString());
            }
            catch (Exception ex)
            {
                Result.Set(context, ex.Message);
            }
        }
    }

    public class emailMove : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_QtyPerBatch { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_SourceBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_DestBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_JunkBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<bool> In_IsLog { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_LogPath { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_EmailTo { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> Result { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                Result.Set(context, "");
                var QtyPerBatch = Int32.Parse(In_QtyPerBatch.Get(context));
                var SourceBoxName = In_SourceBoxName.Get(context);
                var DestBoxName = In_DestBoxName.Get(context);
                var JunkBoxName = In_JunkBoxName.Get(context);
                var LogPath = In_LogPath.Get(context);

                SimpleEmailLib.SimpleEmailClient OClient = new SimpleEmailLib.SimpleEmailClient(SourceBoxName, DestBoxName, JunkBoxName, LogPath, QtyPerBatch);

                OClient.EmailTo = In_EmailTo.Get(context);

                OClient.WriteLog = In_IsLog.Get(context);
                OClient.MoveMail(QtyPerBatch);

            }
            catch (Exception ex)
            {
                Result.Set(context, "Error:" + ex.Message);
            }
        }
    }

    public class DateTimeConvert : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_Date { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> Result { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<List<string>> ExecutionLog { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //added 20211216
            //Create a Log List to store and restore the log to Main Flow
            List<string> LogList = new List<string>();
            string FunctionName = "Execute";

            try
            {
                DateTime dateTime;                

                //var v1 = DateTime.TryParse(In_Date.Get(context), out dateTime) ? dateTime : ConvertStringToDT(In_Date.Get(context));
                var v1 = DateTime.TryParse(In_Date.Get(context), out dateTime) ? dateTime : ConvertStringToDT(In_Date.Get(context),ref LogList);
                if (!(v1 is null))
                {
                    LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> v1 : " + Convert.ToDateTime(v1).ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else
                {
                    LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> v1 : NULL");
                }
                
                Result.Set(context, Convert.ToDateTime(v1).ToString("yyyy-MM-dd HH:mm:ss"));              

            }
            catch (Exception ex)
            {
                Result.Set(context, "Error: " + ex.Message);
                LogList.Add(DateTime.Now.ToString() + " " + FunctionName + "Error: " + ex.Message);
            }
            //added 20211216
            //return Log to main flow
            finally
            {
                ExecutionLog.Set(context, LogList);
            }
        }

        public static bool testDTConvert(string date, string format)
        {
            DateTime dateTime;
            return DateTime.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateTime);
        }


        public static string ConvertChiDateToEngDate(string str)
        {
            Regex rg = new Regex(@"([\u4e00-\u9fa5]{1,2})月");
            Regex rg1 = new Regex(@"月([\u4e00-\u9fa5]{1,3})日");
            Regex rg2 = new Regex(@"([\u4e00-\u9fa5]{2,4})年");

            Match match = rg.Match(str);
            Match match1 = rg1.Match(str);
            Match match2 = rg2.Match(str);

            string month = match.Groups[1].Value;
            string day = match1.Groups[1].Value;
            string year = match2.Groups[1].Value;

            //str = IsChinese(month) ? str.Replace(month, ConvertToChineseNumerals(month, "").ToString()) : str;
            //str = IsChinese(day) ? str.Replace(day, ConvertToChineseNumerals(day, "").ToString()) : str;
            //str = IsChinese(year) ? str.Replace(year, ConvertToChineseNumerals(year, "year").ToString()) : str;

            str = IsChinese(year) ? str.Replace(year, ConvertToChineseNumerals(year, "year").ToString()) : str;
            str = IsChinese(month) ? str.Replace(month, ConvertToChineseNumerals(month, "").ToString()) : str;
            str = IsChinese(day) ? str.Replace(day, ConvertToChineseNumerals(day, "").ToString()) : str;

            //return str;
            return str.Replace("上午", "AM").Replace("下午", "PM");
        }



        public static int ConvertToChineseNumerals(string s, string type)
        {
            if (type.Equals("year"))
            {
                string t = "一二三四五六七八九";   // Index lookup
                int year = 0;
                int thousand = 1000;

                for (int i = 0; i < s.Length; i++)
                {
                    year += (t.IndexOf(s[i]) + 1) * thousand;
                    thousand = thousand / 10;
                }

                return year;
            }
            else
            {
                if (s == "")
                    return 0;   // Recursion termination when input is divisible by 10 (no unit digit character)
                if (s[0] == '零')
                    s = s.Substring(1);   // Ignore '零'
                if (s[0] == '十')
                    s = "一" + s;   // Normalize 10-19 by padding with "一"
                string t = " 一二三四五六七八九";   // Index lookup
                return s.Length < 2
                    ? t.IndexOf(s[0])   // 0-9
                    : (t.IndexOf(s[0]) * (s[1] == '百' ? 100 : 10) + ConvertToChineseNumerals(s.Substring(2), ""));
                // Get the first character's digit, multiply by 100 or 10 depends on 2nd character, and recursively process from 3rd character onwards
            }
        }


        public static bool IsChinese(string text)
        {
            return text.Any(c => (c >= 0x2000) && (c <= 0xFA2D));
        }

        public static string ReplaceWeek(string str)
        {
            string[] arr = { "星期日","星期1","星期2","星期3","星期4","星期5","星期6",
                "週日","週1","週2","週3","週4","週5","週6",
                "周日","周1","周2","周3","周4","周5","周6"};

            string[] arr2 = { "星期日","星期一","星期二","星期三","星期四","星期五","星期六",
                "週日","週一","週二","週三","週四","週五","週六",
                "周日","周一","周二","周三","周四","周五","周六"};

            if (!arr.Any(str.Contains)) return str;

            //return str.Replace(arr.Where(str.Contains).ElementAt(0), "");

            for (int i = 0; i < arr.Length; i++)
            {
                if (str.Contains(arr[i]))
                {
                    return str.Replace(arr[i], arr2[i]);
                }
            }
            return str;
        }


        //public static DateTime? ConvertStringToDT(string pStr)
        //{
        //    DateTime dateTime = DateTime.MinValue;

        //    bool isSuccess = false;

        //    Func<string, List<string>, bool> tryGetDate = (x, y) =>
        //    {
        //        CultureInfo culture = new CultureInfo(x);
        //        DateTimeFormatInfo dateTimeFormatInfo = culture.DateTimeFormat;
        //        List<string> formatStr = new List<string>();

        //        foreach (PropertyInfo item in typeof(DateTimeFormatInfo).GetProperties())
        //        {
        //            if (item.Name.EndsWith("Pattern"))
        //            {
        //                formatStr.Add(item.GetValue(dateTimeFormatInfo).ToString());
        //            }
        //        }

        //        if (y.Count > 0)
        //        {
        //            formatStr.AddRange(y);
        //        }
        //        bool tmpSuccess = DateTime.TryParseExact(pStr, formatStr.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateTime);
        //        if (!tmpSuccess)
        //        {
        //            try
        //            {
        //                dateTime = Convert.ToDateTime(pStr);
        //            }
        //            catch (Exception)
        //            {
        //                tmpSuccess = false;
        //            }
        //        }

        //        return tmpSuccess;
        //    };


        //    pStr = IsChinese(pStr) ? ConvertChiDateToEngDate(pStr) : pStr;

        //    pStr = ReplaceWeek(pStr);

        //    #region en-US
        //    //Special formate
        //    List<string> SpecialFormatus = new List<string>() { "dddd, MMMM d, yyyy h:mm tt", "dddd, MMMM d, yyyy H:mm", "yyyy-MM-dd HH:mm:ss",
        //        "MM/dd/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm:ss", "dd MMMM yyyy HH:mm",
        //        "MM/dd/yyyy hh:mm tt", "dd/MM/yyyy hh:mm tt",
        //        "yyyy-MM-dd HH:mm 'GMT'zzz", "yyyy-MM-dd HH:mm 'UTC'zzz",
        //        "d MMMM yyyy 'at' h:mm:ss tt 'HKT'",
        //        "ddd dd/MM/yyyy h:mm tt","yyyy'年'M'月'd'日'() tth:m",
        //        "dddd, MMMM dd, yyyy h:mm:ss tt", "dddd dd MMMM yyyy 'at' HH:mm", "dddd, dd MMMM yyyy 'at' h:mm tt",
        //        "yyyy-MM-dd'()' HH:mm","yyyy'年'MM'月'dd'日'() tth:mm",",MM月 dd,yyyy HH:mm",
        //        ",MM月 dd,yyyy h:mm tt","yyyy年M月dd日() HH:mm",
        //        "M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()"
        //        /*if have other Special can at here*/};

        //    Dictionary<string, TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones().ToDictionary(z => z.DisplayName);

        //    zones.ToList().ForEach(z => SpecialFormatus.Add("dd MMMM yyyy HH:mm:ss '" + z.Value + "'"));

        //    isSuccess = tryGetDate("en-US", SpecialFormatus);
        //    #endregion

        //    #region zh-CN

        //    if (!isSuccess)
        //    {
        //        var SpecialFormatcn = new List<string>() { "ddd, dd MMM yyyy HH':'mm 'GMT'", "yyyy-MM-dd'(周一)' HH:mm", "yyyy-MM-dd'(周二)' HH:mm", "yyyy-MM-dd'(周三)' HH:mm",
        //            "yyyy-MM-dd'(周四)' HH:mm", "yyyy-MM-dd'(周五)' HH:mm", "yyyy-MM-dd'(周六)' HH:mm", "yyyy-MM-dd'(周日)' HH:mm",
        //            "yyyy'年'MM'月'dd'日'(星期一) tth:mm","yyyy'年'MM'月'dd'日'(星期二) tth:mm","yyyy'年'MM'月'dd'日'(星期三) tth:mm",
        //            "yyyy'年'MM'月'dd'日'(星期四) tth:mm","yyyy'年'MM'月'dd'日'(星期五) tth:mm","yyyy'年'MM'月'dd'日'(星期六) tth:mm",
        //            "yyyy'年'MM'月'dd'日'(星期日) tth:mm",
        //            "周一,MM月 dd,yyyy HH:mm","周二,MM月 dd,yyyy HH:mm","周三,MM月 dd,yyyy HH:mm",
        //            "周四,MM月 dd,yyyy HH:mm","周五,MM月 dd,yyyy HH:mm","周六,MM月 dd,yyyy HH:mm",
        //            "周日,MM月 dd,yyyy HH:mm",
        //            "周一,MM月 dd,yyyy h:mm tt","周二,MM月 dd,yyyy h:mm tt",
        //            "周三,MM月 dd,yyyy h:mm tt","周四,MM月 dd,yyyy h:mm tt","周五,MM月 dd,yyyy h:mm tt",
        //            "周六,MM月 dd,yyyy h:mm tt","周日,MM月 dd,yyyy h:mm tt",
        //            "yyyy年M月dd日(周一) HH:mm","yyyy年M月dd日(周二) HH:mm","yyyy年M月dd日(周三) HH:mm",
        //            "yyyy年M月dd日(周四) HH:mm","yyyy年M月dd日(周五) HH:mm","yyyy年M月dd日(周六) HH:mm",
        //            "yyyy年M月dd日(周日) HH:mm",
        //            "yyyy-MM-dd'()' HH:mm","yyyy'年'MM'月'dd'日'() tth:mm",",MM月 dd,yyyy HH:mm",
        //            ",MM月 dd,yyyy h:mm tt","yyyy年M月dd日() HH:mm",
        //            "M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()"
        //            ,"yyyy'年'M'月'd'日'(星期一) tth:m","yyyy'年'M'月'd'日'(星期二) tth:m","yyyy'年'M'月'd'日'(星期三) tth:m",
        //            "yyyy'年'M'月'd'日'(星期四) tth:m","yyyy'年'M'月'd'日'(星期五) tth:m","yyyy'年'M'月'd'日'(星期六) tth:m",
        //            "yyyy'年'M'月'd'日'(星期日) tth:m",
        //                "星期一, M月 d, yyyy HH:mm tt","星期二, M月 d, yyyy HH:mm tt","星期三, M月 d, yyyy HH:mm tt",
        //                "星期四, M月 d, yyyy HH:mm tt","星期五, M月 d, yyyy HH:mm tt","星期六, M月 d, yyyy HH:mm tt",
        //                "星期日, M月 d, yyyy HH:mm tt"
        //        };

        //        isSuccess = tryGetDate("zh-CN", SpecialFormatcn);
        //    }

        //    #endregion

        //    #region zh-HK


        //    if (!isSuccess)
        //    {
        //        var dayOfWeek = new List<string>() {
        //            "星期一","星期二","星期三","星期四",
        //            "星期五","星期六","星期日"
        //        };

        //        var SpecialFormathk = new List<string>();

        //        foreach (var tt in new[] { "上午", "下午" })
        //        {
        //            dayOfWeek.ForEach(dw =>
        //            {
        //                SpecialFormathk.Add("'" + dw + "', M月 d, yyyy HH:mm " + tt);
        //                SpecialFormathk.Add("'" + dw + "',MM月dd, yyyy HH:mm " + tt);
        //            });
        //        }


        //        SpecialFormathk.AddRange(new List<string>() {"yyyy/MM/dd tt hh:mm",
        //                "cc","M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()",
        //                "M'月'dd'日'yyyy'年'星期一 HH:mm","M'月'dd'日'yyyy'年'星期二 HH:mm","M'月'dd'日'yyyy'年'星期三 HH:mm",
        //                "M'月'dd'日'yyyy'年'星期四 HH:mm","M'月'dd'日'yyyy'年'星期五 HH:mm","M'月'dd'日'yyyy'年'星期六 HH:mm",
        //                "M'月'dd'日'yyyy'年'星期日 HH:mm",
        //                "星期一, M'月' d, yyyy HH:mm","星期二, M'月' d, yyyy HH:mm","星期三, M'月' d, yyyy HH:mm",
        //                "星期四, M'月' d, yyyy HH:mm","星期五, M'月' d, yyyy HH:mm","星期六, M'月' d, yyyy HH:mm",
        //                "星期日, M'月' d, yyyy HH:mm",
        //                "dd/MM/yyyy (週一) HH:mm","dd/MM/yyyy (週二) HH:mm","dd/MM/yyyy (週三) HH:mm",
        //                "dd/MM/yyyy (週四) HH:mm","dd/MM/yyyy (週五) HH:mm","dd/MM/yyyy (週六) HH:mm",
        //                "dd/MM/yyyy (週日) HH:mm",
        //                "yyyy-MM-dd HH:mm:ss (星期一)","yyyy-MM-dd HH:mm:ss (星期二)","yyyy-MM-dd HH:mm:ss (星期三)",
        //                "yyyy-MM-dd HH:mm:ss (星期四)","yyyy-MM-dd HH:mm:ss (星期五)","yyyy-MM-dd HH:mm:ss (星期六)",
        //                "yyyy-MM-dd HH:mm:ss (星期日)"
        //        });



        //        isSuccess = tryGetDate("zh-HK", SpecialFormathk);
        //    }

        //    #endregion

        //    #region Other CULTURE
        //    //can try other CULTURE   
        //    #endregion

        //    if (isSuccess)
        //    {
        //        return dateTime;
        //    }

        //    return null;
        //}

        public static DateTime? ConvertStringToDT(string pStr, ref List<string> LogList)
        {
            DateTime dateTime = DateTime.MinValue;

            bool isSuccess = false;

            string FunctionName = "ConvertStringToDT";

            LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> Start Convert");

            bool temp = false;

            Func<string, List<string>, bool> tryGetDate = (x, y) =>
            {
                CultureInfo culture = new CultureInfo(x);
                DateTimeFormatInfo dateTimeFormatInfo = culture.DateTimeFormat;
                List<string> formatStr = new List<string>();

                foreach (PropertyInfo item in typeof(DateTimeFormatInfo).GetProperties())
                {
                    if (item.Name.EndsWith("Pattern"))
                    {
                        formatStr.Add(item.GetValue(dateTimeFormatInfo).ToString());
                    }
                }

                if (y.Count > 0)
                {
                    formatStr.AddRange(y);
                }
                bool tmpSuccess = DateTime.TryParseExact(pStr, formatStr.ToArray(), CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out dateTime);
                if (!tmpSuccess)
                {
                    try
                    {
                        dateTime = Convert.ToDateTime(pStr);
                    }
                    catch (Exception)
                    {
                        tmpSuccess = false;
                    }
                }
                temp = tmpSuccess;
                return tmpSuccess;
            };

            LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> tmpSuccess : " + temp.ToString());

            pStr = IsChinese(pStr) ? ConvertChiDateToEngDate(pStr) : pStr;

            LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> pStr after is Chinese / ConvertChiDateToEngDate : " + pStr);

            pStr = ReplaceWeek(pStr);

            LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> pStr after is ReplaceWeek : " + pStr);

            #region en-US
            //Special formate
            List<string> SpecialFormatus = new List<string>() { "dddd, MMMM d, yyyy h:mm tt", "dddd, MMMM d, yyyy H:mm", "yyyy-MM-dd HH:mm:ss",
                "MM/dd/yyyy HH:mm:ss", "dd/MM/yyyy HH:mm:ss", "dd MMMM yyyy HH:mm",
                "MM/dd/yyyy hh:mm tt", "dd/MM/yyyy hh:mm tt",
                "yyyy-MM-dd HH:mm 'GMT'zzz", "yyyy-MM-dd HH:mm 'UTC'zzz",
                "d MMMM yyyy 'at' h:mm:ss tt 'HKT'",
                "ddd dd/MM/yyyy h:mm tt","yyyy'年'M'月'd'日'() tth:m",
                "dddd, MMMM dd, yyyy h:mm:ss tt", "dddd dd MMMM yyyy 'at' HH:mm", "dddd, dd MMMM yyyy 'at' h:mm tt",
                "yyyy-MM-dd'()' HH:mm","yyyy'年'MM'月'dd'日'() tth:mm",",MM月 dd,yyyy HH:mm",
                ",MM月 dd,yyyy h:mm tt","yyyy年M月dd日() HH:mm",
                "M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()"
                /*if have other Special can at here*/
                //added 20211213, 3 formats
                ,"dddd, MMMM d, yyyy 'at' H:mm tt","dddd, MMMM d, yyyy 'at' HH:mm tt","yyyy MMM. d (ddd.) H:mm"
                //added 20211230, 2 formats
                ,"dddd, MMMM d, yyyy 'at' h:mm tt"
                ,"'At:'yyyy MMM. d (ddd.) H:mm"
            };

            Dictionary<string, TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones().ToDictionary(z => z.DisplayName);

            zones.ToList().ForEach(z => SpecialFormatus.Add("dd MMMM yyyy HH:mm:ss '" + z.Value + "'"));

            isSuccess = tryGetDate("en-US", SpecialFormatus);

            LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> isSuccess(en-US) : " + isSuccess.ToString());
            #endregion

            #region zh-CN

            if (!isSuccess)
            {
                var SpecialFormatcn = new List<string>() { "ddd, dd MMM yyyy HH':'mm 'GMT'", "yyyy-MM-dd'(周一)' HH:mm", "yyyy-MM-dd'(周二)' HH:mm", "yyyy-MM-dd'(周三)' HH:mm",
                    "yyyy-MM-dd'(周四)' HH:mm", "yyyy-MM-dd'(周五)' HH:mm", "yyyy-MM-dd'(周六)' HH:mm", "yyyy-MM-dd'(周日)' HH:mm",
                    "yyyy'年'MM'月'dd'日'(星期一) tth:mm","yyyy'年'MM'月'dd'日'(星期二) tth:mm","yyyy'年'MM'月'dd'日'(星期三) tth:mm",
                    "yyyy'年'MM'月'dd'日'(星期四) tth:mm","yyyy'年'MM'月'dd'日'(星期五) tth:mm","yyyy'年'MM'月'dd'日'(星期六) tth:mm",
                    "yyyy'年'MM'月'dd'日'(星期日) tth:mm",
                    "周一,MM月 dd,yyyy HH:mm","周二,MM月 dd,yyyy HH:mm","周三,MM月 dd,yyyy HH:mm",
                    "周四,MM月 dd,yyyy HH:mm","周五,MM月 dd,yyyy HH:mm","周六,MM月 dd,yyyy HH:mm",
                    "周日,MM月 dd,yyyy HH:mm",
                    "周一,MM月 dd,yyyy h:mm tt","周二,MM月 dd,yyyy h:mm tt",
                    "周三,MM月 dd,yyyy h:mm tt","周四,MM月 dd,yyyy h:mm tt","周五,MM月 dd,yyyy h:mm tt",
                    "周六,MM月 dd,yyyy h:mm tt","周日,MM月 dd,yyyy h:mm tt",
                    "yyyy年M月dd日(周一) HH:mm","yyyy年M月dd日(周二) HH:mm","yyyy年M月dd日(周三) HH:mm",
                    "yyyy年M月dd日(周四) HH:mm","yyyy年M月dd日(周五) HH:mm","yyyy年M月dd日(周六) HH:mm",
                    "yyyy年M月dd日(周日) HH:mm",
                    "yyyy-MM-dd'()' HH:mm","yyyy'年'MM'月'dd'日'() tth:mm",",MM月 dd,yyyy HH:mm",
                    ",MM月 dd,yyyy h:mm tt","yyyy年M月dd日() HH:mm",
                    "M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()"
                    ,"yyyy'年'M'月'd'日'(星期一) tth:m","yyyy'年'M'月'd'日'(星期二) tth:m","yyyy'年'M'月'd'日'(星期三) tth:m",
                    "yyyy'年'M'月'd'日'(星期四) tth:m","yyyy'年'M'月'd'日'(星期五) tth:m","yyyy'年'M'月'd'日'(星期六) tth:m",
                    "yyyy'年'M'月'd'日'(星期日) tth:m",
                        "星期一, M月 d, yyyy HH:mm tt","星期二, M月 d, yyyy HH:mm tt","星期三, M月 d, yyyy HH:mm tt",
                        "星期四, M月 d, yyyy HH:mm tt","星期五, M月 d, yyyy HH:mm tt","星期六, M月 d, yyyy HH:mm tt",
                        "星期日, M月 d, yyyy HH:mm tt"
                    //added 20211213, 22 formats
                    ,"yyyy年M月d日 星期一 tt HH:mm","yyyy年M月d日 星期二 tt HH:mm","yyyy年M月d日 星期三 tt HH:mm","yyyy年M月d日 星期四 tt HH:mm"
                    ,"yyyy年M月d日 星期五 tt HH:mm","yyyy年M月d日 星期六 tt HH:mm","yyyy年M月d日 星期日 tt HH:mm"
                    ,"yyyy'年'M'月'd'日'('星期一') HH:mm","yyyy'年'M'月'd'日'('星期二') HH:mm","yyyy'年'M'月'd'日'('星期三') HH:mm"
                    ,"yyyy'年'M'月'd'日'('星期四') HH:mm","yyyy'年'M'月'd'日'('星期五') HH:mm","yyyy'年'M'月'd'日'('星期六') HH:mm","yyyy'年'M'月'd'日'('星期日') HH:mm"
                    ,"yyyy'年'M'月'd'日' tt hh:mm"
                    ,"星期一, M'月' d, yyyy H:mm","星期二, M'月' d, yyyy H:mm","星期三, M'月' d, yyyy H:mm"
                    ,"星期四, M'月' d, yyyy H:mm","星期五, M'月' d, yyyy H:mm","星期六, M'月' d, yyyy H:mm","星期日, M'月' d, yyyy H:mm"
                };

                isSuccess = tryGetDate("zh-CN", SpecialFormatcn);
                LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> isSuccess(zh-CN) : " + isSuccess.ToString());
            }

            #endregion

            #region zh-HK


            if (!isSuccess)
            {
                var dayOfWeek = new List<string>() {
                    "星期一","星期二","星期三","星期四",
                    "星期五","星期六","星期日"
                };

                var SpecialFormathk = new List<string>();

                foreach (var tt in new[] { "上午", "下午" })
                {
                    dayOfWeek.ForEach(dw =>
                    {
                        SpecialFormathk.Add("'" + dw + "', M月 d, yyyy HH:mm " + tt);
                        SpecialFormathk.Add("'" + dw + "',MM月dd, yyyy HH:mm " + tt);
                    });
                }


                SpecialFormathk.AddRange(new List<string>() {"yyyy/MM/dd tt hh:mm",
                        "cc","M'月'd'日'yyyy'年' HH:mm",", M'月' d, yyyy HH:mm","dd/MM/yyyy () HH:mm","yyyy-MM-dd HH:mm:ss ()",
                        "M'月'dd'日'yyyy'年'星期一 HH:mm","M'月'dd'日'yyyy'年'星期二 HH:mm","M'月'dd'日'yyyy'年'星期三 HH:mm",
                        "M'月'dd'日'yyyy'年'星期四 HH:mm","M'月'dd'日'yyyy'年'星期五 HH:mm","M'月'dd'日'yyyy'年'星期六 HH:mm",
                        "M'月'dd'日'yyyy'年'星期日 HH:mm",
                        "星期一, M'月' d, yyyy HH:mm","星期二, M'月' d, yyyy HH:mm","星期三, M'月' d, yyyy HH:mm",
                        "星期四, M'月' d, yyyy HH:mm","星期五, M'月' d, yyyy HH:mm","星期六, M'月' d, yyyy HH:mm",
                        "星期日, M'月' d, yyyy HH:mm",
                        "dd/MM/yyyy (週一) HH:mm","dd/MM/yyyy (週二) HH:mm","dd/MM/yyyy (週三) HH:mm",
                        "dd/MM/yyyy (週四) HH:mm","dd/MM/yyyy (週五) HH:mm","dd/MM/yyyy (週六) HH:mm",
                        "dd/MM/yyyy (週日) HH:mm",
                        "yyyy-MM-dd HH:mm:ss (星期一)","yyyy-MM-dd HH:mm:ss (星期二)","yyyy-MM-dd HH:mm:ss (星期三)",
                        "yyyy-MM-dd HH:mm:ss (星期四)","yyyy-MM-dd HH:mm:ss (星期五)","yyyy-MM-dd HH:mm:ss (星期六)",
                        "yyyy-MM-dd HH:mm:ss (星期日)"
                });



                isSuccess = tryGetDate("zh-HK", SpecialFormathk);

                LogList.Add(DateTime.Now.ToString() + " " + FunctionName + " >>> isSuccess(zh-HK) : " + isSuccess.ToString());
            }

            #endregion

            #region Other CULTURE
            //can try other CULTURE   
            #endregion

            if (isSuccess)
            {
                return dateTime;
            }

            return null;
        }
    }

    public class PDFDecrypt : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> FilenameSource { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> FilenameDest { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> FolderPathIn { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> FolderPathOut { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> Password { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<int> StartPage { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<int> EndPage { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> ErrMsg { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                ErrMsg.Set(context, "");
                string filenameSource = FilenameSource.Get(context);
                string filenameDest = FilenameDest.Get(context);
                string folderPathIn = FolderPathIn.Get(context);
                string folderPathOut = FolderPathOut.Get(context);
                string password = Password.Get(context);
                int startpage = StartPage.Get(context);
                int endpage = EndPage.Get(context);
                PDFLib PDFLib = new PDFLib(filenameSource, filenameDest, folderPathIn, folderPathOut);

                PDFLib.DecryptPdfWithPage(password, startpage, endpage);
            }
            catch(Exception ex)
            {
                ErrMsg.Set(context, ex.Message);
            }
        }
    }
}
