﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.IO;

namespace SimpleEmailLib
{
    public class SimpleEmailClient
    {
        private Outlook.Application OutlookApp;
        private Outlook.NameSpace OutlookNS;
        private Outlook.MAPIFolder sourceBox;
        private Outlook.MAPIFolder destBox;
        private Outlook.MAPIFolder junkBox;

        public string logfile = "MoveEmailTestLog";

        public Int32 QtyPerBatch { get; set; }
        public string SourceBoxName { get; set; }
        public string DestBoxName { get; set; }
        public string JunkBoxName { get; set; }
        public string LogPath { get; set; }
        public string EmailTo { get; set; }
        public Boolean WriteLog { get; set; }
        public int CntOfRemainingEmail { get; private set; }

        public SimpleEmailClient(string srcFolder, string tgtFolder, string jugFolder, string tgtLogPath)
        {
            QtyPerBatch = 10;
            SourceBoxName = srcFolder;
            DestBoxName = tgtFolder;
            JunkBoxName = jugFolder;
            LogPath = tgtLogPath;
            CntOfRemainingEmail = 0;
        }

        public SimpleEmailClient(string srcFolder, string tgtFolder, string jugFolder, string tgtLogPath, Int32 MailRequired)
        {
            QtyPerBatch = MailRequired;
            SourceBoxName = srcFolder;
            DestBoxName = tgtFolder;
            JunkBoxName = jugFolder;
            LogPath = tgtLogPath;
            CntOfRemainingEmail = 0;
        }

        public SimpleEmailClient()
        {
            QtyPerBatch = 10;
            SourceBoxName = "Inbox";
            DestBoxName = "Temp";
            JunkBoxName = "Deleted Items";
            LogPath = @"C:\CIGL\Log\";
            CntOfRemainingEmail = 0;
        }

        private void Init()
        {
            if (OutlookApp == null)
            {
                OutlookApp = new Outlook.Application();
                OutlookNS = OutlookApp.GetNamespace("MAPI");

                Outlook.Folder root = OutlookApp.Session.DefaultStore.GetRootFolder() as Outlook.Folder;
                EnumerateFolders(root);

                if (sourceBox == null)
                {
                    writelog(LogPath, DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + String.Format("Mail Folder {0} is not found", SourceBoxName), WriteLog);
                    throw new System.Exception(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + String.Format("Mail Folder {0} is not found", SourceBoxName));
                }

                if (destBox == null)
                {
                    writelog(LogPath, DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + String.Format("Mail Folder {0} is not found", DestBoxName), WriteLog);
                    throw new System.Exception(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss] : ") + String.Format("Mail Folder {0} is not found", DestBoxName));
                }

                if (junkBox == null)
                {
                    writelog(LogPath, DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + String.Format("Mail Folder {0} is not found", JunkBoxName), WriteLog);
                    throw new System.Exception(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss] : ") + String.Format("Mail Folder {0} is not found", JunkBoxName));
                }
            }
        }

        public void MoveMail(Int32 MailRequired)
        {
            CntOfRemainingEmail = 0;
            Int32 TtlMailHandle = 0;
            Console.WriteLine("LogPath: " + LogPath);
            if (!string.IsNullOrWhiteSpace(LogPath))
            {
                logfile += DateTime.Now.ToString("_yyyyMMdd") + ".txt";
                LogPath += logfile;

                Console.WriteLine("LogPath: " + LogPath);
            }
            Init();

            if (sourceBox.Items != null && sourceBox.Items.Count > 0)
            {
                writelog(LogPath, "Found " + sourceBox.Items.Count.ToString() + " Items from MailFolder:" + sourceBox.Name, WriteLog);
                Outlook.Items Items = sourceBox.Items.Restrict("[Unread]=true");
                Items.Sort("[ReceivedTime]", true);
                if (MailRequired == 0)
                {
                    TtlMailHandle = Items.Count;
                }
                else
                {
                    if (Items.Count > MailRequired)
                        TtlMailHandle = MailRequired;
                    else
                        TtlMailHandle = Items.Count;
                }
                writelog(LogPath, "Retrieve " + TtlMailHandle.ToString() + " Items from MailFolder:" + sourceBox.Name, WriteLog);
                writelog(LogPath, "Sort MailFolder:" + sourceBox.Name + " Items", WriteLog);

                Console.WriteLine("TtlMailHandle:" + TtlMailHandle);
                Console.WriteLine("Items.Count:" + Items.Count);

                var LoopTimes = Items.Count - TtlMailHandle;

                for (Int32 i = Items.Count; i > LoopTimes; i--)
                {
                    try
                    {
                        if (i < 1)
                        {
                            break;
                        }
                        if (Items[i] is Outlook.MailItem)
                        {
                            Outlook.MailItem M = Items[i];
                            writelog(LogPath, "Move item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(destBox);
                        }
                        else if (Items[i] is Outlook.SharingItem)
                        {
                            Outlook.SharingItem M = Items[i];
                            writelog(LogPath, "Forward SharingItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            Outlook._SharingItem replayMail = M.Forward();

                            replayMail.To = EmailTo;
                            replayMail.Subject = M.Subject;
                            replayMail.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                            replayMail.Send();
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.MeetingItem)
                        {
                            Outlook.MeetingItem M = Items[i];
                            writelog(LogPath, "Move MeetingItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.TaskItem)
                        {
                            Outlook.TaskItem M = Items[i];
                            writelog(LogPath, "Move TaskItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.AppointmentItem)
                        {
                            Outlook.AppointmentItem M = Items[i];
                            writelog(LogPath, "Move AppointmentItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.ContactItem)
                        {
                            Outlook.ContactItem M = Items[i];
                            writelog(LogPath, "Move ContactItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.DistListItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move DistListItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.DocumentItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move DocumentItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.JournalItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move JournalItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.NoteItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move NoteItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.PostItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move PostItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.RemoteItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move RemoteItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.ReportItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move ReportItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.SimpleItems)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move SimpleItems item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else if (Items[i] is Outlook.StorageItem)
                        {
                            Outlook.DistListItem M = Items[i];
                            writelog(LogPath, "Move StorageItem item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(junkBox);
                            LoopTimes--;
                        }
                        else
                        {
                            var M = Items[i];
                            writelog(LogPath, "Move unknown item " + i.ToString() + " Message -> " + M.Subject + "  ok.", WriteLog);
                            M.Move(destBox);
                        }

                    }
                    catch (Exception Ex)
                    {
                        writelog(LogPath, "Move item " + i.ToString() + ", Subject:" + Items[i].Subject + ". Message is failed. Error :" + Ex.Message, true);
                        throw Ex;
                    }
                }
            }
        }

        private void writelog(string LogPath, string entry, Boolean enableLog)
        {
            if (enableLog)
            {
                using (StreamWriter sw = File.AppendText(LogPath))
                {
                    sw.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + entry);
                    Console.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + entry);
                }
            }
        }

        private void EnumerateFolders(Outlook.Folder folder)
        {
            Outlook.Folders childFolders = folder.Folders;

            if (childFolders.Count > 0)
            {
                foreach (Outlook.Folder childFolder in childFolders)
                {
                    if (childFolder.Name == DestBoxName)
                    {
                        destBox = childFolder;
                        Console.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "destBox Folder:" + DestBoxName + " found and set !!!!");
                        if (!string.IsNullOrWhiteSpace(LogPath))
                        {
                            using (StreamWriter sw = File.AppendText(LogPath))
                            {
                                sw.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "destBox Folder:" + DestBoxName + " found and set !!!!");
                            }
                        }
                    }

                    if (childFolder.Name == SourceBoxName)
                    {
                        sourceBox = childFolder;
                        Console.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "sourceBox Folder:" + SourceBoxName + " found and set !!!!");
                        if (!string.IsNullOrWhiteSpace(LogPath))
                        {
                            using (StreamWriter sw = File.AppendText(LogPath))
                            {
                                sw.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "sourceBox Folder:" + SourceBoxName + " found and set !!!!");
                            }
                        }
                    }

                    if (childFolder.Name == JunkBoxName)
                    {
                        junkBox = childFolder;
                        Console.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "junkBox Folder:" + JunkBoxName + " found and set !!!!");
                        if (!string.IsNullOrWhiteSpace(LogPath))
                        {
                            using (StreamWriter sw = File.AppendText(LogPath))
                            {
                                sw.WriteLine(DateTime.Now.ToString("[yyyyMMdd hh:mm:ss]") + " : " + "junkBox Folder:" + JunkBoxName + " found and set !!!!");
                            }
                        }
                    }

                    // Write the folder path.
                    //Console.WriteLine(childFolder.FolderPath);
                    // Call EnumerateFolders using childFolder.
                    EnumerateFolders(childFolder);
                }
            }
        }
    }
}
