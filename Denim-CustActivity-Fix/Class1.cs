﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Net;
using System.Text.RegularExpressions;

namespace Denim_CustActivity_Fix
{

    public class emailMoveHotFix : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_QtyPerBatch { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_SourceBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_DestBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_JunkBoxName { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<bool> In_IsLog { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_LogPath { get; set; }

        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> In_EmailTo { get; set; }

        [Category("Output")]
        [RequiredArgument]
        public OutArgument<string> Result { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                Result.Set(context, "");
                var QtyPerBatch = Int32.Parse(In_QtyPerBatch.Get(context));
                var SourceBoxName = In_SourceBoxName.Get(context);
                var DestBoxName = In_DestBoxName.Get(context);
                var JunkBoxName = In_JunkBoxName.Get(context);
                var LogPath = In_LogPath.Get(context);

                SimpleEmailLib.SimpleEmailClient OClient = new SimpleEmailLib.SimpleEmailClient(SourceBoxName, DestBoxName, JunkBoxName, LogPath, QtyPerBatch);

                OClient.EmailTo = In_EmailTo.Get(context);

                OClient.WriteLog = In_IsLog.Get(context);
                OClient.MoveMail(QtyPerBatch);

            }
            catch (Exception ex)
            {
                Result.Set(context, "Error:" + ex.Message);
            }
        }
    }

}
